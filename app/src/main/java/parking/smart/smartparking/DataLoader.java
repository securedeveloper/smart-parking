package parking.smart.smartparking;

import android.os.AsyncTask;

import org.json.JSONObject;

import parking.smart.smartparking.models.DataDownloader;

public class DataLoader extends AsyncTask<String, Void, String> {
    DataDownloader dataDownloader;

    public DataLoader(DataDownloader dataDownloader) {
        this.dataDownloader = dataDownloader;
    }

    @Override
    protected String doInBackground(String... strings) {
        return null;
    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);

        this.dataDownloader.onDataDownloaded(data);
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);

        this.dataDownloader.onDataLoadingFailed(s);
    }
}
