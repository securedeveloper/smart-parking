package parking.smart.smartparking.models;

public class PlaceProperties {
    int BEZIRK;
    int BEZIRK2;
    String WEBLINK1;
    String ZEITRAUM;
    String DAUER;
    String WEBLINK2;
    String GUELTIG_VON;
    int SE_SDO_ROWID;
    String SE_ANNO_CAD_DATA;

    public PlaceProperties() {
    }

    public PlaceProperties(int BEZIRK, int BEZIRK2, String WEBLINK1, String ZEITRAUM, String DAUER, String WEBLINK2, String GUELTIG_VON, int SE_SDO_ROWID, String SE_ANNO_CAD_DATA) {
        this.BEZIRK = BEZIRK;
        this.BEZIRK2 = BEZIRK2;
        this.WEBLINK1 = WEBLINK1;
        this.ZEITRAUM = ZEITRAUM;
        this.DAUER = DAUER;
        this.WEBLINK2 = WEBLINK2;
        this.GUELTIG_VON = GUELTIG_VON;
        this.SE_SDO_ROWID = SE_SDO_ROWID;
        this.SE_ANNO_CAD_DATA = SE_ANNO_CAD_DATA;
    }

    public int getBEZIRK() {
        return BEZIRK;
    }

    public void setBEZIRK(int BEZIRK) {
        this.BEZIRK = BEZIRK;
    }

    public int getBEZIRK2() {
        return BEZIRK2;
    }

    public void setBEZIRK2(int BEZIRK2) {
        this.BEZIRK2 = BEZIRK2;
    }

    public String getWEBLINK1() {
        return WEBLINK1;
    }

    public void setWEBLINK1(String WEBLINK1) {
        this.WEBLINK1 = WEBLINK1;
    }

    public String getZEITRAUM() {
        return ZEITRAUM;
    }

    public void setZEITRAUM(String ZEITRAUM) {
        this.ZEITRAUM = ZEITRAUM;
    }

    public String getDAUER() {
        return DAUER;
    }

    public void setDAUER(String DAUER) {
        this.DAUER = DAUER;
    }

    public String getWEBLINK2() {
        return WEBLINK2;
    }

    public void setWEBLINK2(String WEBLINK2) {
        this.WEBLINK2 = WEBLINK2;
    }

    public String getGUELTIG_VON() {
        return GUELTIG_VON;
    }

    public void setGUELTIG_VON(String GUELTIG_VON) {
        this.GUELTIG_VON = GUELTIG_VON;
    }

    public int getSE_SDO_ROWID() {
        return SE_SDO_ROWID;
    }

    public void setSE_SDO_ROWID(int SE_SDO_ROWID) {
        this.SE_SDO_ROWID = SE_SDO_ROWID;
    }

    public String getSE_ANNO_CAD_DATA() {
        return SE_ANNO_CAD_DATA;
    }

    public void setSE_ANNO_CAD_DATA(String SE_ANNO_CAD_DATA) {
        this.SE_ANNO_CAD_DATA = SE_ANNO_CAD_DATA;
    }
}
