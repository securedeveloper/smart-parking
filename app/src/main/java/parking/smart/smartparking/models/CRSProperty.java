package parking.smart.smartparking.models;

public class CRSProperty {
    String name;

    public CRSProperty() {
    }

    public CRSProperty(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
