package parking.smart.smartparking.models;

public class CRS {
    String type;
    CRSProperty properties;

    public CRS() {
    }

    public CRS(String type, CRSProperty properties) {
        this.type = type;
        this.properties = properties;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CRSProperty getProperties() {
        return properties;
    }

    public void setProperties(CRSProperty properties) {
        this.properties = properties;
    }
}
