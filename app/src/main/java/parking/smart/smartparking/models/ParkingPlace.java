package parking.smart.smartparking.models;

public class ParkingPlace {
    String id;
    String type;
    String geometry_name;
    Geometry geometry;
    PlaceProperties properties;

    public ParkingPlace() {
    }

    public ParkingPlace(String id, String type, String geometry_name, Geometry geometry, PlaceProperties properties) {
        this.id = id;
        this.type = type;
        this.geometry_name = geometry_name;
        this.geometry = geometry;
        this.properties = properties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGeometry_name() {
        return geometry_name;
    }

    public void setGeometry_name(String geometry_name) {
        this.geometry_name = geometry_name;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public PlaceProperties getProperties() {
        return properties;
    }

    public void setProperties(PlaceProperties properties) {
        this.properties = properties;
    }
}
