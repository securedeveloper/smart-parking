package parking.smart.smartparking.models;

import java.util.ArrayList;

public class ParkingData {
    String type;
    int totalFeatures;
    ArrayList<ParkingPlace> features;
    CRS crs;

    public ParkingData() {
    }

    public ParkingData(String type, int totalFeatures, ArrayList<ParkingPlace> features, CRS crs) {
        this.type = type;
        this.totalFeatures = totalFeatures;
        this.features = features;
        this.crs = crs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTotalFeatures() {
        return totalFeatures;
    }

    public void setTotalFeatures(int totalFeatures) {
        this.totalFeatures = totalFeatures;
    }

    public ArrayList<ParkingPlace> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<ParkingPlace> features) {
        this.features = features;
    }

    public CRS getCrs() {
        return crs;
    }

    public void setCrs(CRS crs) {
        this.crs = crs;
    }

    @Override
    public String toString() {
        return "ParkingData{" +
                "type='" + type + '\'' +
                ", totalFeatures=" + totalFeatures +
                '}';
    }
}
