package parking.smart.smartparking.models;


public interface DataDownloader {
    void onDataDownloaded(String data);

    void onDataLoadingFailed(String error);
}
