package parking.smart.smartparking;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.owl93.dpb.CircularProgressView;

public class MainActivity extends AppCompatActivity {

    CircularProgressView circularProgressView;
    ImageView splashCar;
    Button startButton;

    private static final int LOCATION_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        circularProgressView = findViewById(R.id.splashProgressBar);
        splashCar = findViewById(R.id.splashCar);
        startButton = findViewById(R.id.splashButton);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_car);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashCar.setVisibility(View.INVISIBLE);
                startButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        splashCar.startAnimation(animation);

        new ProgressLoader().execute(circularProgressView);

        startButton.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkPermission();
            } else {
                startMaps();
            }
        });
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            startMaps();
        } else {
            serviceUnavailableDialog();
        }
    }

    public void serviceUnavailableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Permissions");
        dialogBuilder.setIcon(R.drawable.ic_info_icon);
        dialogBuilder.setView(R.layout.permissions_explaination);
        dialogBuilder.setNegativeButton("Don't Allow", (dialog, which) -> {
            System.exit(1);
        });

        dialogBuilder.setPositiveButton("To Permissions", (dialog, which) -> {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startMaps();
            } else {
                Toast.makeText(this, "Location permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void startMaps() {
        startActivity(new Intent(MainActivity.this, MapActivity.class));
        finish();
    }

    public void enableActivity() {
        startButton.setEnabled(true);
    }

    @SuppressLint("StaticFieldLeak")
    private class ProgressLoader extends AsyncTask<CircularProgressView, Void, Void> {

        @Override
        protected Void doInBackground(CircularProgressView... progressViews) {
            for (int i = 1; i <= 100; i++) {
                try {
                    Thread.sleep(20);
                    progressViews[0].setProgress(i);
                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            enableActivity();
        }
    }
}